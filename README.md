# Web automation scripts

A collection of (testcafe) scripts to automate web processes (filling out forms, doing batch jobs, ...)

## Installation

```bash
npm install testcafe node-xlsx
```

## Run

To run the scripts it is necessary to have the list of ressources prepared as an Excel file.

| Script                | Data source          | Sheet  | Column names       |
|-----------------------|----------------------|--------|--------------------|
| convert_to_qti21.js   | data/ressources.xlsx | Sheet1 | title, last_access |
| access_all_courses.js | data/courses.xlsx    | Sheet1 | title              |

Run the script with the following command:

```bash
USER='olatadmin' PASS='secret0' ./node_modules/.bin/testcafe --skip-js-errors chrome convert_to_qti21.js
```

It is advisable to ignore JavaScript errors. OLAT sometimes spits out JavaScript errors,
even if it is not functional impaired.

## Documentation

Getting started with testcafe: https://devexpress.github.io/testcafe/documentation/getting-started/
