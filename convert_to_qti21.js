/*
 * Testcafe test to convert all test resources (with test in title) to QTI2.1 in OLAT
 *
 * Getting started: https://devexpress.github.io/testcafe/documentation/getting-started/
 *
 * Run:
 *
 * USER='adminuser' PASS='topsecret' /usr/local/bin/testcafe chrome convert_to_qti21.js
 *
 */
import xlsx from 'node-xlsx';
import { Selector } from 'testcafe';
import { Role } from 'testcafe';

// Selectors
const searchButton = Selector('.btn-primary').withText('Suchen');
const typeDropdown = Selector('.dropdown-toggle').withText('Alle anzeigen').nth(0);
const testType = Selector('input').withAttribute('data-value', 'Test').parent();
const convertToQTI2 = Selector('a').withText('In QTI 2.1 konvertieren');
const closeTestElem = Selector('.o_breadcrumb_close').child('a');

fixture `Getting Started`
    .page `https://rc.olat.uzh.ch/auth/MyCoursesSite/0`;


const olatUser = Role('https://rc.olat.uzh.ch/dmz/', async t => {
	await t
        .click('.o_icon_provider_olat')
		.typeText('#o_fiooolat_login_name', process.env.USER)
		.typeText('#o_fiooolat_login_pass', process.env.PASS)
		.click('#o_fiooolat_login_button');
}, { preserveUrl: true });

// get data from excel
// see https://dev-tester.com/data-driven-testing-in-testcafe-part-2-csv-and-excel/
const excelFile = xlsx.parse("./data/ressources.xlsx");
const excelSheet = excelFile.find(sheets => sheets.name == "Sheet1");
const excelSheetData = excelSheet.data;
const headers = excelSheetData.shift();
const userData = excelSheetData.map((row) => {
	const resource = {}
	row.forEach((data, idx) => resource[headers[idx]] = data);
	return resource;
});

userData.forEach(resource => {
	test(`Converted: ${resource.title} last accessed ${resource.last_access}`, async t => {
		await t
//			.useRole(olatUser)
			.click('.o_icon_provider_olat')
			.typeText('#o_fiooolat_login_name', process.env.USER)
			.typeText('#o_fiooolat_login_pass', process.env.PASS)
			.click('#o_fiooolat_login_button')
			.click(Selector('a').withText('Autorenbereich'))
			.click(Selector('a').withText('Suchmaske'))
			.click('.o_sel_flexi_extendedsearch')
			.click('.o_sel_repo_search_displayname')
			.pressKey('ctrl+a delete')
			.typeText('.o_sel_repo_search_displayname', '"' + resource.title + '"')
			.click(typeDropdown)
			.click(testType)
			.click(searchButton)		
			// title of test
			.click(Selector('.o_coursetable tbody').find('tr').nth(0).find('td').nth(3).child('a'))
			.click(Selector('a.o_sel_repository_tools').withText('Administration'));

		if (await convertToQTI2.exists) {
			await convertToQTI2.with({ visibilityCheck: true })();
			await t
				.click(convertToQTI2)
				.click('.o_sel_author_create_submit');
			if (await closeTestElem.exists) {
				await t
				    .click('.o_alert_close'); /*
					.doubleClick(closeTestElem)
					.click(Selector('a').withText('Abbrechen')); */
			} else if (await Selector('h1').withText('Ups, da ging was schief!').exists) {
				console.log('Failed: RedScreen!');
				await t
					.click(Selector('a').withText('Zurück'))
					.useRole(olatUser)
					.click(Selector('a').withText('Abbrechen'));
			}
		} else {
			console.log('Failed: No button!');
			await t
				.doubleClick(closeTestElem);
		}
		await t
			.click('#o_sel_navbar_my_menu_caret')
			.click('a.o_logout');
	});
});